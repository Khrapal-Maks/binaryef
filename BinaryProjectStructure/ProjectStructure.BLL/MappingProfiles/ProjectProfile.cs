﻿using AutoMapper;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.DAL.Entities;

namespace ProjectStructure.BLL.MappingProfiles
{
    public sealed class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectDTO>();

            CreateMap<ProjectDTO, Project>();

            CreateMap<NewProjectDTO, Project>();
        }
    }
}
