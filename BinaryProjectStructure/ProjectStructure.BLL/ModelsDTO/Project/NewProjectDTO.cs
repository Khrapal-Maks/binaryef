﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProjectStructure.BLL.ModelsDTO
{
    public sealed class NewProjectDTO
    {
        [Required]
        public int AuthorId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        public DateTime Deadline { get; set; }
    }
}
