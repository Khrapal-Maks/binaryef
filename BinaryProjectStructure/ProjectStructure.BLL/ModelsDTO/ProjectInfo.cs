﻿using ProjectStructure.DAL.Entities;

namespace ProjectStructure.BLL.ModelsDTO
{
    public struct ProjectInfo
    {
        public Project Project { get; set; }
        public Tasks TaskLongDescription { get; set; }
        public Tasks TasksShortName { get; set; }
        public int? CountUsersInTeamProject { get; set; }
    }
}
