﻿using ProjectStructure.BLL.ModelsDTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public interface IUsersService
    {
        Task<IEnumerable<UserDTO>> GetAllUsers();

        Task<UserDTO> CreateUser(NewUserDTO item);

        Task<UserDTO> GetUser(int value);

        Task<UserDTO> UpdateUser(UserDTO item);

        void DeleteUser(int value);
    }
}
