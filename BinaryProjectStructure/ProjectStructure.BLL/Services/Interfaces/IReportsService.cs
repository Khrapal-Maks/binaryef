﻿using ProjectStructure.BLL.ModelsDTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public interface IReportsService
    {
        Task<List<Tuple<ProjectDTO, int>>> GetAllTasksInProjectsByAuthor(int value);

        Task<List<TasksDTO>> GetAllTasksOnPerformer(int value);

        Task<List<Tuple<int, string>>> GetAllTasksThatFinished(int value);

        Task<List<Tuple<int, string, List<UserDTO>>>> GetAllUsersOldestThanTenYears();

        Task<List<Tuple<string, List<TasksDTO>>>> SortAllUsersFirstNameAndSortTaskOnName();

        Task<UserInfo?> GetStructUserById(int value);

        Task<List<ProjectInfo>> GetStructAllProjects();
    }
}
