﻿using AutoMapper;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public sealed class TasksService : BaseService, ITasksService
    {
        private readonly IUnitOfWork _database;

        public TasksService(IUnitOfWork context, IMapper mapper) : base(context, mapper)
        {
            _database = context;
        }

        public async Task<IEnumerable<TasksDTO>> GetAllTasks()
        {
            return await Task.Run(() =>
            {
                var tasks = _database.GetRepositoryTasks.GetAll();

                return _mapper.Map<IEnumerable<TasksDTO>>(tasks);
            });
        }

        public async Task<TasksDTO> GetTask(int id)
        {
            return await Task.Run(() =>
            {
                var task = _database.GetRepositoryTasks.Get(id);

                return _mapper.Map<TasksDTO>(task);
            });
        }

        public async Task<TasksDTO> CreateTask(NewTaskDTO newTask)
        {
            return await Task.Run(() =>
            {
                var taskEntity = _mapper.Map<Tasks>(newTask);

                var createToTask = _database.GetRepositoryTasks.Create(taskEntity);
                _database.Save();

                var createdTask = _database.GetRepositoryTasks.Get(createToTask.Id);

                return _mapper.Map<TasksDTO>(createdTask);
            });
        }

        public async Task<TasksDTO> UpdateTask(TasksDTO task)
        {
            return await Task.Run(() =>
            {
                var updateTask = _mapper.Map<Tasks>(task);

                _database.GetRepositoryTasks.Update(task.Id, updateTask);

                _database.Save();

                var updatedTask = _database.GetRepositoryTasks.Get(task.Id);

                return _mapper.Map<TasksDTO>(updatedTask);
            });
        }

        public void DeleteTask(int id)
        {
            _database.GetRepositoryTasks.Delete(id);
            _database.Save();
        }
    }
}
