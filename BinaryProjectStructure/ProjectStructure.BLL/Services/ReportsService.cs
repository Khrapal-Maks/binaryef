﻿using AutoMapper;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public sealed class ReportsService : BaseService, IReportsService
    {
        private readonly IUnitOfWork _database;

        public ReportsService(IUnitOfWork context, IMapper mapper) : base(context, mapper)
        {
            _database = context;
        }

        public async Task<List<Tuple<ProjectDTO, int>>> GetAllTasksInProjectsByAuthor(int authorId)
        {
            return await Task.Run(() => _database.GetRepositoryProjects.GetAll().Where(x => x.AuthorId == authorId)
                .GroupJoin(_database.GetRepositoryTasks.GetAll(),
                projects => projects.Id,
                tasks => tasks.ProjectId,
                (pr, ts) => new { Projects = pr, Tasks = ts })
                .Select(x => Tuple.Create(new ProjectDTO
                {
                    Id = x.Projects.Id,
                    AuthorId = x.Projects.AuthorId ?? 0,
                    CreatedAt = x.Projects.CreatedAt,
                    Deadline = x.Projects.Deadline,
                    Description = x.Projects.Description,
                    Name = x.Projects.Name,
                    TeamId = x.Projects.TeamId ?? 0
                }, x.Tasks.Count())).ToList());
        }

        public async Task<List<TasksDTO>> GetAllTasksOnPerformer(int performerId)
        {
            int lengthNameTask = 45;

            var result = await Task.Run(() => _database.GetRepositoryTasks.GetAll().Where(x => x.PerformerId == performerId & x.Name.Length < lengthNameTask).ToList());

            return _mapper.Map<IEnumerable<TasksDTO>>(result).ToList();
        }

        public async Task<List<Tuple<int, string>>> GetAllTasksThatFinished(int performerId)
        {
            int year = 2021;

            return await Task.Run(() => _database.GetRepositoryTasks.GetAll()
                  .Where(x => x.PerformerId == performerId)
                  .Where(x => x.FinishedAt.HasValue && x.FinishedAt.Value.Year == year)
                  .Select(x => Tuple.Create(x.Id, x.Name)).ToList());
        }

        public async Task<List<Tuple<int, string, List<UserDTO>>>> GetAllUsersOldestThanTenYears()
        {
            int ageUser = 10;

            return await Task.Run(() => _database.GetRepositoryTeams.GetAll()
                .GroupJoin(_database.GetRepositoryUsers.GetAll()
                .Where(x => x.BirthDay.AddYears(ageUser) < DateTime.Now)
                .OrderByDescending(x => x.RegisteredAt),
                team => team.Id,
                performer => performer.TeamId,
                (tm, pr) => new
                {
                    Team = tm,
                    Users = pr
                }).Select(x => Tuple.Create(x.Team, _mapper.Map<IEnumerable<UserDTO>>(x.Users).ToList())).Distinct()
                .Select(x => Tuple.Create(x.Item1.Id, x.Item1.Name, x.Item2.ToList())).ToList());
        }

        public async Task<List<Tuple<string, List<TasksDTO>>>> SortAllUsersFirstNameAndSortTaskOnName()
        {
            return await Task.Run(() => _database.GetRepositoryUsers.GetAll()
                .OrderBy(x => x.FirstName)
                .GroupJoin(_database.GetRepositoryTasks.GetAll().OrderByDescending(x => x.Name.Length),
                performer => performer.Id,
                task => task.PerformerId,
                (pr, ts) => new { Performer = pr, Tasks = ts })
                .Select(x => Tuple.Create(x.Performer.FirstName, _mapper.Map<IEnumerable<TasksDTO>>(x.Tasks).ToList()))
                .Where(x => x.Item2.Any()).ToList());
        }

        public async Task<UserInfo?> GetStructUserById(int id)
        {
            var date = new DateTime(0001, 01, 01, 00, 00, 00);

            var user = await Task.Run(() => _database.GetRepositoryUsers.Get(id) ?? null);

            if (user == null)
            {
                return null;
            }

            var project = _database.GetRepositoryProjects.FindIEnumerable(x => x.AuthorId == user.Id).OrderByDescending(x => x.CreatedAt).FirstOrDefault() ?? null;

            int countTasksInProject = 0;

            if (project != null)
            {
                countTasksInProject = _database.GetRepositoryTasks.FindIEnumerable(x => x.ProjectId == project.Id).Where(x => x.CreatedAt != date).Count();
            }

            int countTasksInWork = _database.GetRepositoryTasks.GetAll().Count(x => x.PerformerId == user.Id & x.FinishedAt != date);

            var task = _database.GetRepositoryTasks.FindIEnumerable(x => x.PerformerId == user.Id).OrderByDescending(x => x.FinishedAt - x.CreatedAt).FirstOrDefault() ?? null;

            return new UserInfo
            {
                User = user,
                Project = project,
                CountTasksInProject = countTasksInProject,
                CountTasksInWork = countTasksInWork,
                Tasks = task
            };
        }

        public async Task<List<ProjectInfo>> GetStructAllProjects()
        {
            int descriptionLength = 20;
            int countTask = 3;

            return await Task.Run(() => _database.GetRepositoryProjects.GetAll()
                .GroupJoin(_database.GetRepositoryTasks.GetAll(),
                project => project.Id,
                task => task.ProjectId,
                (project, task) => new { Projects = project, Tasks = task })
                .Join(_database.GetRepositoryTeams.GetAll(),
                project => project.Projects.TeamId,
                team => team.Id,
                (project, team) => new { Project = project, Team = team })
                .GroupJoin(_database.GetRepositoryUsers.GetAll(),
                project => project.Team.Id,
                user => user.TeamId,
                (project, user) => new ProjectInfo
                {
                    Project = project.Project.Projects,
                    TaskLongDescription = project.Project.Tasks.OrderBy(x => x.Description.Length).FirstOrDefault() ?? null,
                    TasksShortName = project.Project.Tasks.OrderBy(x => x.Name.Length).FirstOrDefault() ?? null,
                    CountUsersInTeamProject =
                    project.Project.Projects.Description.Length > descriptionLength | project.Project.Tasks.Count() < countTask
                    ? user.Count() : 0
                }).Distinct().ToList());
        }
    }
}
