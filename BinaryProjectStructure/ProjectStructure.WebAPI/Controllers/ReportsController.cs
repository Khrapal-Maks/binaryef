﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.BLL.Services;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.WebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class ReportsController : ControllerBase
    {
        private readonly IReportsService _reportsService;
        private readonly ILogger<Project> _logger;

        public ReportsController(IReportsService reportsService, ILogger<Project> logger)
        {
            _reportsService = reportsService;
            _logger = logger;
        }

        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<List<Tuple<ProjectDTO, int>>>> GetAllTasksInProjectsByAuthor(int id)
        {
            var result = await _reportsService.GetAllTasksInProjectsByAuthor(id);

            if (result == null)
            {
                return NoContent();
            }

            return Ok(result);
        }

        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<List<TasksDTO>>> GetAllTasksOnPerformer(int id)
        {
            var result = await _reportsService.GetAllTasksOnPerformer(id);

            if (result == null)
            {
                return NoContent();
            }

            return Ok(result);
        }

        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<List<Tuple<int, string>>>> GetAllTasksThatFinished(int id)
        {
            var result = await _reportsService.GetAllTasksThatFinished(id);

            if (result == null)
            {
                return NoContent();
            }

            return Ok(result);
        }

        [HttpGet("[action]")]
        public async Task<ActionResult<List<Tuple<int, string, List<UserDTO>>>>> GetAllUsersOldestThanTenYears()
        {
            var result = await _reportsService.GetAllUsersOldestThanTenYears();

            if (result == null)
            {
                return NoContent();
            }

            return Ok(result);
        }

        [HttpGet("[action]")]
        public async Task<ActionResult<List<Tuple<string, List<TasksDTO>>>>> SortAllUsersFirstNameAndSortTaskOnName()
        {
            var result = await _reportsService.SortAllUsersFirstNameAndSortTaskOnName();

            if (result == null)
            {
                return NoContent();
            }

            return Ok(result);
        }

        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<UserInfo>> GetStructUserById(int id)
        {
            var result = await _reportsService.GetStructUserById(id);

            if (result == null)
            {
                return NoContent();
            }

            return Ok(result);
        }

        [HttpGet("[action]")]
        public async Task<ActionResult<List<ProjectInfo>>> GetStructAllProjects()
        {
            var result = await _reportsService.GetStructAllProjects();

            return Ok(result);
        }
    }
}
