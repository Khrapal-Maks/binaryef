﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.BLL.Services;
using ProjectStructure.DAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamController : ControllerBase
    {
        private readonly ITeamService _teamService;
        private readonly ILogger<Team> _logger;

        public TeamController(ITeamService teamService, ILogger<Team> logger)
        {
            _teamService = teamService;
            _logger = logger;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TeamDTO>>> GetAllTeams()
        {
            return Ok(await _teamService.GetAllTeams());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<TeamDTO>>> GetTeam(int id)
        {
            var team = await _teamService.GetTeam(id);

            if (team == null)
            {
                return NotFound();
            }

            return Ok(team);
        }

        [HttpPost]
        public async Task<ActionResult<TeamDTO>> CreateTeam([FromBody] NewTeamDTO newTeam)
        {
            return Ok(await _teamService.CreateTeam(newTeam));
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<TeamDTO>> UpdateTeam(int id, [FromBody] TeamDTO team)
        {
            if (id != team.Id)
            {
                return BadRequest();
            }

            var teamToUpdate = await _teamService.GetTeam(id);

            if (teamToUpdate == null)
            {
                return NotFound();
            }

            return Ok(await _teamService.UpdateTeam(team));
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteTeam(int id)
        {
            var teamToDelete = await _teamService.GetTeam(id);

            if (teamToDelete == null)
            {
                return NotFound();
            }

            _teamService.DeleteTeam(id);
            return Ok();
        }
    }
}
