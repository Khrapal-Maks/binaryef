﻿using Bogus;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;

namespace ProjectStructure.DAL.Context
{
    public static class ModelBuilderExtensions
    {
        private const int ENTITY_TEAMS_COUNT = 14;
        private const int ENTITY_USERS_COUNT = 80;
        private const int ENTITY_PROJECT_COUNT = 30;
        private const int ENTITY_TASKS_COUNT = 120;
        private const int PROBABILITY_OF_NULL = 10;

        public static void Configure(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Project>().Property(c => c.AuthorId)
               .HasColumnName("CuratorProjectID");

            modelBuilder.Entity<Project>()
               .HasMany<Tasks>()
               .WithOne()
               .HasForeignKey(r => r.ProjectId)
               .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Team>()
               .HasMany<User>()
               .WithOne()
               .HasForeignKey(r => r.TeamId)
               .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Project>()
               .HasOne<Team>()
               .WithMany()
               .HasForeignKey(r => r.TeamId)
               .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Project>()
               .HasOne<User>()
               .WithMany()
               .HasForeignKey(r => r.AuthorId)
               .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Tasks>()
               .HasOne<User>()
               .WithMany()
               .HasForeignKey(r => r.PerformerId)
               .OnDelete(DeleteBehavior.SetNull);
        }

        public static void Seed(this ModelBuilder modelBuilder)
        {
            ICollection<Team> teams = GenerateRandomTeams();
            ICollection<User> users = GenerateRandomUsers();
            ICollection<Project> projects = GenerateRandomProjects();
            ICollection<Tasks> tasks = GenerateRandomTasks();

            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<Tasks>().HasData(tasks);
        }

        public static ICollection<Team> GenerateRandomTeams()
        {
            int teamId = 1;

            Faker<Team> teamsFake = new Faker<Team>()
                .RuleFor(t => t.Id, f => teamId++)
                .RuleFor(t => t.Name, f => f.Name.LastName() + ", " + f.Name.LastName() + " and " + f.Name.LastName())
                .RuleFor(t => t.CreatedAt, f => f.Date.Past(1));

            return teamsFake.Generate(ENTITY_TEAMS_COUNT);
        }

        public static ICollection<User> GenerateRandomUsers()
        {
            int userId = 1;

            Faker<User> usersFake = new Faker<User>()
                .RuleFor(u => u.Id, f => userId++)
                .RuleFor(u => u.FirstName, f => f.Name.FirstName())
                .RuleFor(u => u.LastName, f => f.Name.LastName())
                .RuleFor(u => u.RegisteredAt, f => f.Date.Past(2))
                .RuleFor(u => u.BirthDay, f => f.Date.Between(f.Date.Past(40), f.Date.Past(8)))
                .RuleFor(u => u.Email, f => f.Internet.Email())
                .RuleFor(u => u.TeamId, f => f.Random.Number(1, ENTITY_TEAMS_COUNT));

            return usersFake.Generate(ENTITY_USERS_COUNT);
        }

        public static ICollection<Project> GenerateRandomProjects()
        {
            int projectId = 1;

            DateTime startDateCreatedAtProject = new(2020, 1, 1, 1, 1, 1);
            DateTime endDateCreatedAtProject = new(2021, 3, 1, 1, 1, 1);

            Faker<Project> projectFake = new Faker<Project>()
                .RuleFor(p => p.Id, f => projectId++)
                .RuleFor(p => p.AuthorId, f => f.Random.Number(1, ENTITY_USERS_COUNT))
                .RuleFor(p => p.TeamId, f => f.Random.Number(1, ENTITY_TEAMS_COUNT))
                .RuleFor(p => p.Name, f => f.Lorem.Sentence(7))
                .RuleFor(p => p.Description, f => f.Lorem.Text())
                .RuleFor(p => p.CreatedAt, f => f.Date.Between(startDateCreatedAtProject, endDateCreatedAtProject))
                .RuleFor(p => p.Deadline, f => f.Date.Between(endDateCreatedAtProject, f.Date.Future(1)));

            return projectFake.Generate(ENTITY_PROJECT_COUNT);
        }

        public static ICollection<Tasks> GenerateRandomTasks()
        {
            int taskId = 1;

            DateTime startDateCreatedAtTask = new(2021, 1, 1, 1, 1, 1);
            DateTime endDateCreatedAtTask = new(2021, 5, 1, 1, 1, 1);
            DateTime startDateFinishedAtTask = new(2021, 6, 1, 1, 1, 1);
            DateTime endDateFinishedAtTask = new(2021, 7, 1, 1, 1, 1);

            Faker<Tasks> tasksFake = new Faker<Tasks>()
                .RuleFor(t => t.Id, f => taskId++)
                .RuleFor(t => t.ProjectId, f => f.Random.Number(1, ENTITY_PROJECT_COUNT))
                .RuleFor(t => t.PerformerId, f => f.Random.Number(1, ENTITY_USERS_COUNT))
                .RuleFor(t => t.Name, f => f.Lorem.Sentence(7))
                .RuleFor(t => t.Description, f => f.Lorem.Text())
                .RuleFor(t => t.State, f => f.PickRandom<TaskState>())
                .RuleFor(t => t.CreatedAt, f => f.Date.Between(startDateCreatedAtTask, endDateCreatedAtTask))
                .RuleFor(t => t.FinishedAt, f => GetRandomNumberForNullable() < PROBABILITY_OF_NULL ? f.Date.Between(startDateFinishedAtTask, endDateFinishedAtTask) : null);

            return tasksFake.Generate(ENTITY_TASKS_COUNT);
        }

        private static int GetRandomNumberForNullable()
        {
            Random random = new();
            return random.Next(0, 100);
        }
    }
}
