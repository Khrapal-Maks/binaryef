﻿using ProjectStructure.DAL.Entities;
using System;

namespace ProjectStructure.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        GenericRepository<Project> GetRepositoryProjects { get; }

        GenericRepository<Tasks> GetRepositoryTasks { get; }

        GenericRepository<Team> GetRepositoryTeams { get; }

        GenericRepository<User> GetRepositoryUsers { get; }

        void Save();
    }
}
